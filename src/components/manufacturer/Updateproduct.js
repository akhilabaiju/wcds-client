import React, { useState, useEffect } from 'react';
import UpdateForm from './UpdateForm';
import { useNavigate } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import { toast } from 'react-toastify';
import productService from '../../services/productService';

const Updateproduct = () => {
    const [data, setData] = useState(null);
    const productId = localStorage.getItem("productId");
    const navigate = useNavigate();

    useEffect(() => {
        fetchData();
    }, []); // Fetch data when the component mounts

    const fetchData = async () => {
        try {
            const response = await productService.getProductsById(productId);
            const result = await response.json();
            setData(result);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

   

    return (
        <div className="create-user">
          
            {data && <UpdateForm initialValues={data}  />}
        </div>
    );
};

export default Updateproduct;
