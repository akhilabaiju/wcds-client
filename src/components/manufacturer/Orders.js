
import React, { useState, useEffect } from 'react';
import Link from '@mui/material/Link';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Title from './Title';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import orderService from '../../services/orderService';
import productService from '../../services/productService';
import userService from '../../services/userService';
import manufacturerService from '../../services/manufacturerService';
// Generate Order Data
function createData(id, date, name, shipTo, quantity, amount) {
  return { id, date, name, shipTo, quantity, amount };
}

// Helper function to parse and format the date
function formatCreatedAtDate(createdAt) {
  const date = new Date(createdAt);
  return date.toLocaleDateString(); // Adjust the formatting as needed
}

export default function Orders() {
  const id = localStorage.getItem("id");
  const [orders, setOrders] = useState([]);
  const [users, setUsers] = useState({});
  const [products, setProducts] = useState({});
  const [manufacturers, setManufacturers] = useState({});
  useEffect(() => {
    fetchOrders();
  }, []);

  const fetchOrders = async () => {
    try {
      const response = await orderService.getManufactureOrders(id);
      if (!response.ok) {
        throw new Error('Failed to fetch orders');
      }
      const data = await response.json();
      setOrders(data);
      const productResponse = await productService.getAllProducts();
            if (!productResponse.ok) {
              throw new Error('Failed to fetch manufacturers');
            }
            const productsData = await productResponse.json();
            const productsMap = {};
            productsData.forEach(products => {
              productsMap[products._id] = products.productName;
            });
            setProducts(productsMap);
      // *****************
            const userResponse = await userService.getAllUsers();
            if (!userResponse.ok) {
              throw new Error('Failed to fetch manufacturers');
            }
            const userData = await userResponse.json();
             const userMap = {};
             userData.forEach(users => {
               userMap[users._id] = users.firstName+" "+users.lastName;
             });
            setUsers(userMap);


            // Fetch manufacturers
        const manufacturerResponse = await manufacturerService.getAllManufacturers();
        if (!manufacturerResponse.ok) {
          throw new Error('Failed to fetch manufacturers');
        }
        const manufacturerData = await manufacturerResponse.json();
        const manufacturerMap = {};
        manufacturerData.forEach(manufacturer => {
          manufacturerMap[manufacturer._id] = manufacturer.firstName + " " + manufacturer.lastName;
        });
        setManufacturers(manufacturerMap);



    } catch (error) {
      console.error(error.message);
    }
  };

  return (
    <Grid item xs={12}>
      <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
        <React.Fragment>
          <Title>Recent Orders</Title>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell>Date</TableCell>
                <TableCell>Name</TableCell>
                <TableCell>Ship From</TableCell>
                <TableCell>Ship To</TableCell>
                <TableCell>Quantity</TableCell>
                <TableCell align="right">Sale Amount</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {orders && orders.map((row) => ( 
                  <TableRow key={row.id}>
                  <TableCell>{formatCreatedAtDate(row.createdAt)}</TableCell>
                  <TableCell>{products[row.productId]}</TableCell>
                  <TableCell>{manufacturers[row.manufactureId]}</TableCell>
             
<TableCell>
  {row.vendorId === "No Vendor" ? users[row.userId] : null}

  {row.userId === row.vendorId ? users[row.userId] : null}
</TableCell>

                  <TableCell>{row.quantity}</TableCell>
                  <TableCell align="right">{`Rs.${row.amount}`}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          
        </React.Fragment>
      </Paper>
    </Grid>
  );
}
