import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Title from './Title';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import productService from '../../services/productService';
import { useState,useEffect } from 'react';
import EditIcon from '@mui/icons-material/Edit';
import { Avatar } from '@mui/material';
import { useNavigate } from 'react-router-dom';
const API_URL = process.env.REACT_APP_IMG_URL;

export default function Listproducts() {
  const navigate = useNavigate();
    const [data, setData] = useState(null);
    const id = localStorage.getItem('id');
    const [img,setImg] = useState();
    useEffect(() => {
        const fetchData = async () => {
          try {
                const response = await productService.getProducts(id);
                if (!response.ok) {
                  throw new Error('No data found');
                }        
                const result = await response.json();
                setData(result);
                //setImg(response.data[0].img)
               // console.log("hi == "+response.data[0].img)
          } catch (error) {
            console.error('Error fetching data:', error.message);
        }
        };
    
        fetchData();
      }, [id]);
      
      
       const handleEdit = (productId) => {
        // Navigate to the product addition page, passing the productId as a parameter
        localStorage.setItem("productId",productId)
        navigate(`/manufacturer/Updateproduct`);
    }
  return (
    <Grid item xs={12}>
                <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
    <React.Fragment>
      <Title>My Products </Title>
      <Table size="small">
        <TableHead>
          <TableRow>
          <TableCell>Product </TableCell>
            <TableCell>Product Name</TableCell>
            <TableCell>Product Weight(in Liters)</TableCell>
            <TableCell>Product Price(Rs.)</TableCell>
            
            <TableCell >Available Numbers</TableCell>
            <TableCell ></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        
          {data && data.map(row => (
             <TableRow key={row.id}>
              <TableCell > {row.imageUrl && 
              <img style={{ width: '50px', height: '50px' }}
              src={
                  `${API_URL}/` + row.imageUrl.replace(/src\\public\\/, '')
                }
            />} </TableCell>
             
              <TableCell>{row.productName}</TableCell>
              <TableCell>{`${row.productWeight}`}</TableCell>
              <TableCell>{`₹${row.productPrice}`}</TableCell>
              <TableCell>{row.pNumber<=0? "No Stock": row.pNumber}</TableCell>
              <TableCell onClick={e => handleEdit(row._id)} value={row._id}> 
                        <Avatar sx={{ bgcolor: "primary.main" ,width: 24, height: 24 }}><EditIcon/></Avatar>
                          </TableCell>
            </TableRow>
          ))}
       
        </TableBody>
      </Table>
      {/* <Link color="primary" href="#" onClick={preventDefault} sx={{ mt: 3 }}>
        See more orders
      </Link> */}
    </React.Fragment>
    </Paper></Grid>
  );
}