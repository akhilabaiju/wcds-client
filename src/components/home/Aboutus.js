// Aboutus.js
import React from 'react';
import Box from '@mui/material/Box';
import logo from '../../assets/images/aboutus.jpg';

const Aboutus = ({ headerWidth }) => {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: headerWidth,
      }}
    >
      <img src={logo} alt="landing" style={{ width: '100%' }} />
    </Box>
  );
};

export default Aboutus;
