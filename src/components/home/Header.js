import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import { useNavigate } from 'react-router-dom';

const Header = () => {
const navigate =useNavigate();

const handleHome= ()=>{
       
  navigate('/homepage')
}
    const handleLogin= ()=>{
    
        navigate('/login')
    }
    const handleProducts= ()=>{
        navigate('/productslist')
    }
    
    const handleAboutus= ()=>{
      navigate('/aboutus')
  }
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
           
          </IconButton>
          <Typography variant="h6" sx={{ flexGrow: .02 }} >
            WCDS
          </Typography>
          <Typography variant="h6" sx={{ flexGrow: .02 }} >
          <Button color="inherit" onClick={handleHome}>HOME</Button>
          </Typography>
          <Typography variant="h6" sx={{ flexGrow: .02 }} >
          <Button color="inherit" onClick={handleProducts}>PRODUCTS</Button>
          </Typography>
          <Typography variant="h6"  sx={{ flexGrow: 1 }}>
          <Button color="inherit" onClick={handleAboutus}>AboutUs</Button>
          </Typography>
          <Button color="inherit" onClick={handleLogin}>Login</Button>
          
           
        </Toolbar>
      </AppBar>
    </Box>
  );
}
export default Header