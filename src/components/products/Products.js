import React,{ useState,useEffect } from 'react';
import ProductCard from '../cart/ProductCard';
import ProductList from './ProductList';
import { Container, Grid } from '@mui/material';
import productService from '../../services/productService';

const Products = () => {
  const [cart, setCart] = useState([]);
  const [data, setData] = useState(null);
  useEffect(() => {
    const fetchData = async () => {
      try {
            const response = await productService.getAllProducts();
            if (!response.ok) {
              throw new Error('Failed to fetch data');
            }        
            const result = await response.json();
           console.log("img- "+result[0].imageUrl)
            setData(result);
      } catch (error) {
        console.error('Error fetching data:', error.message);
    }

    };
    fetchData();
  }, []);





  const handleAddToCart = (product) => {
    setCart((prevCart) => {
      const isProductInCart = prevCart.find((item) => item.id === product._id);
      if (isProductInCart) {
        return (prevCart)&&prevCart.map((item) =>
          item.id === product._id ? { ...item, quantity: item.quantity + 1 } : item
        );
      }
     
      //localStorage.setItem("cart",cart);
      return [...prevCart, { ...product, quantity: 1 }];
    });
  };

  return (  
     <Container component="div" >
      <ProductList/>
     
      <Grid container spacing={4}>
        {(data)&&data.map((product) => (
          <Grid item key={product._id} xs={12} sm={6} md={4} >
            <ProductCard product={product} height={500} />
           </Grid>
        ))}
      </Grid>
     
    </Container>
  

  );
};

export default Products;