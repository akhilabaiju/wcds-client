// import React, { useState, useEffect } from 'react';
// import { Paper, Grid, Table, TableHead, TableBody, TableRow, TableCell } from '@mui/material';
// import orderService from '../../services/orderService';
// import manufacturerService from '../../services/manufacturerService';
// import productService from '../../services/productService';

// const Home = () => {
//   const [data, setData] = useState(null);
//   const [manufacturers, setManufacturers] = useState({});
//   const [products, setProducts] = useState({});

//   useEffect(() => {
//     const fetchData = async () => {
//       try {
//         // Fetch orders
//         const id = localStorage.getItem("id");
//         const response = await orderService.getOrders(id);
//         if (!response.ok) {
//           throw new Error('Failed to fetch data');
//         }
//         const result = await response.json();
//         setData(result);

//         // Fetch manufacturers
//         const manufacturerResponse = await manufacturerService.getAllManufacturers();
//         if (!manufacturerResponse.ok) {
//           throw new Error('Failed to fetch manufacturers');
//         }
//         const manufacturerData = await manufacturerResponse.json();
//         const manufacturerMap = {};
//         manufacturerData.forEach(manufacturer => {
//           manufacturerMap[manufacturer._id] = manufacturer.firstName + " " + manufacturer.lastName;
//         });
//         setManufacturers(manufacturerMap);

//         // Fetch products
//         const productResponse = await productService.getAllProducts();
//         if (!productResponse.ok) {
//           throw new Error('Failed to fetch products');
//         }
//         const productsData = await productResponse.json();
//         const productsMap = {};
//         productsData.forEach(product => {
//           productsMap[product._id] = {
//             name: product.productName,
//             weight: product.productWeight
//           };
//         });
//         setProducts(productsMap);
//       } catch (error) {
//         console.error('Error fetching data:', error.message);
//       }
//     };

//     fetchData();
//   }, []);

//   return (
//     <div>
//       <Grid item xs={12}>
//         <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
//           <Table size="small">
//             <TableHead>
//               <TableRow>
//                 <TableCell>Product Name</TableCell>
//                 <TableCell>Product Weight(in Liters)</TableCell>
//                 <TableCell>Manufacture Name</TableCell>
//                 <TableCell>Product Price(Rs.)</TableCell>
//                 <TableCell>Quantity</TableCell>
//                 <TableCell>Amount Paid</TableCell>
//                 <TableCell>Date Of Order</TableCell>
//                 <TableCell>Delivery Date</TableCell>
//                 <TableCell>Vendor</TableCell>
//               </TableRow>
//             </TableHead>
//             <TableBody>
//               {data && data.map(row => (
//                 <TableRow key={row._id}>
//                   <TableCell>{products[row.productId]?.name}</TableCell>
//                   <TableCell>{products[row.productId]?.weight}</TableCell>
//                   <TableCell>{manufacturers[row.manufactureId]}</TableCell>
//                   <TableCell>{row.amount}</TableCell>
//                   <TableCell>{row.quantity}</TableCell>
//                   <TableCell>{row.totalAmount}</TableCell>
//                   <TableCell>{row.createdAt}</TableCell>
//                   <TableCell>With in 48 hours</TableCell>
//                   {/* <TableCell>{vendor[row.vendorId]}</TableCell> */}
//                 </TableRow>
//               ))}
//             </TableBody>
//           </Table>
//         </Paper>
//       </Grid>
//     </div>
//   );
// };

// export default Home;

import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import productService from '../../services/productService';
import { useState,useEffect } from 'react';
import EditIcon from '@mui/icons-material/Edit';
import { Avatar } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import vendorService from '../../services/vendorService';

export default function Home() {
  const navigate = useNavigate();
    const [data, setData] = useState(null);
    const id = localStorage.getItem('id');
    const [img,setImg] = useState();
    useEffect(() => {
        const fetchData = async () => {
          try {
                const response = await vendorService.getProductsbyId(id);
                if (!response.ok) {
                  throw new Error('Failed to fetch data');
                }        
                const result = await response.json();
                setData(result);
                //setImg(response.data[0].img)
               // console.log("hi == "+response.data[0].img)
          } catch (error) {
            console.error('Error fetching data:', error.message);
        }
        };
    
        fetchData();
      }, [id]);
      
      
       const handleEdit = (productId) => {
        // Navigate to the product addition page, passing the productId as a parameter
        localStorage.setItem("productId",productId)
        navigate(`/manufacturer/Updateproduct`);
    }
  return (
    <Grid item xs={12}>
                <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
    <React.Fragment>
      My Products 
      <Table size="small">
        <TableHead>
          <TableRow>
          <TableCell>Product </TableCell>
            <TableCell>Product Name</TableCell>
            <TableCell>Product Weight(in Liters)</TableCell>
            <TableCell>Product Price(Rs.)</TableCell>
            
            <TableCell >Available Numbers</TableCell>
            <TableCell ></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        
          {data && data.map(row => (
             <TableRow key={row.id}>
              <TableCell > {row.imageUrl && 
              <img style={{ width: '50px', height: '50px' }}
                src={
                    'http://localhost:3001//' + row.imageUrl.replace(/src\\public\\/, '')
                }
            />} </TableCell>
             
              <TableCell>{row.productName}</TableCell>
              <TableCell>{`${row.productWeight}`}</TableCell>
              <TableCell>{`₹${row.productPrice}`}</TableCell>
              <TableCell>{row.pNumber<=0? "No Stock": row.pNumber}</TableCell>
              {/* <TableCell onClick={e => handleEdit(row._id)} value={row._id}> 
                        <Avatar sx={{ bgcolor: "primary.main" ,width: 24, height: 24 }}><EditIcon/></Avatar>
                          </TableCell> */}
            </TableRow>
          ))}
       
        </TableBody>
      </Table>
      {/* <Link color="primary" href="#" onClick={preventDefault} sx={{ mt: 3 }}>
        See more orders
      </Link> */}
    </React.Fragment>
    </Paper></Grid>
  );
}
