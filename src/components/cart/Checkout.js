import React, { useState,useEffect } from 'react';
//import { Stepper, Step, StepLabel, Button, Typography, Paper, Grid } from '@mui/material';
import {
  Stepper, Step, StepLabel,
   Typography,
  Button,
  Grid,
  List,
  ListItem,
  ListItemText,
  Divider,
  Paper,
  TextField,
  Box,
  Radio, RadioGroup, FormControlLabel,
  } from "@mui/material";
  import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import PublicIcon from '@mui/icons-material/Public';
import PublicOutlinedIcon from '@mui/icons-material/PublicOutlined';
import ContactPhoneIcon from '@mui/icons-material/ContactPhone';
import ContactPhoneOutlinedIcon from '@mui/icons-material/ContactPhoneOutlined';
import { Link } from 'react-router-dom';
import { styled } from '@mui/material/styles';
import userService from '../../services/userService';
import PaymentForm from './PaymentForm';
import { useNavigate } from "react-router-dom";
import UpdateForm from '../user/UpdateForm';
import cartService from '../../services/cartService';
import AddressPage from '../user/AddressPage';
import orderService from '../../services/orderService';
import Addresscomponent from './Addresscomponent';
import { keyframes } from '@emotion/react';

const steps = [ 'Shipping Information', 'Payment Information', 'Confirm Order'];

const userId = localStorage.getItem('id');
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));
const Checkout = () => {
  const [activeStep, setActiveStep] = useState(0);
  const [data, setData] = useState(null);
  const [cartItems, setCartItems] = useState([]);
  const navigate = useNavigate();
  const vendorId="No Vendor";
  const updateCart=()=>{
    navigate('/user/userheader/userwish');
}
const userId=localStorage.getItem("id");
  useEffect(() => {
    const fetchData = async () => {
    try {
        const response = await userService.getUserById(userId);
        const result = await response.json();
        setData(result);
       const res =await cartService.getCart(userId,vendorId);
       if (!res.ok) {
          throw new Error('Failed to fetch data');
        }
        const result1 = await res.json();
        //console.log("cart= "+result1)
        setCartItems(result1);
    } catch (error) {
        console.error('Error fetching data:', error);
    }
};
fetchData();
}, []);

 // const navigate = useNavigate();
   const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleCheckout = async() =>{
    const remove=await orderService.placeOrder(cartItems);
   
    localStorage.removeItem("addressId")
    localStorage.removeItem("cart")
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  }
  return (
    <Grid>
    <Paper sx={{ padding: 2 ,marginLeft:15, marginRight:15, marginTop:5}}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>
        {activeStep === steps.length ? (
          <div>
            <Typography sx={{ mt: 2, mb: 1 }}>Thank you for your order!</Typography>
            {/* <Button onClick={() => setActiveStep(0)}>Place Another Order</Button> */}
            <Link to="/user/userheader/userwish">Place Another Order</Link>
          </div>
        ) : (
          <div>
            <Typography sx={{ mt: 2, mb: 1 }}>
              {/* Step {activeStep + 1} */}
              </Typography>
            {/* Render step content based on activeStep */}
            {getStepContent(activeStep, data, cartItems)}
            <div sx={{ display: 'flex', justifyContent: 'flex-end', mt: 2 }}>
              {activeStep !== 0 && (
                <Button onClick={handleBack} size='small' sx={{ mr: 1 }}>
                  Back
                </Button>
              )}
              {activeStep != steps.length - 1 &&(
                 <Button onClick={handleNext} size='small' sx={{ mr: 1 }}>
                 Proceed
               </Button>
              )}
               {activeStep === steps.length - 1 &&(
                 <Button onClick={handleCheckout} size='small' sx={{ mr: 1 }}>
                 Place Order
               </Button>
              )}
             
              {/* <Button variant="contained" size='small' onClick={handleNext}>
                {activeStep != steps.length - 1 ? 'Place Order' : 'Proceed'}
              </Button> */}
            </div>
          </div>
        )}
      </div>
    </Paper>
    </Grid>
  );
};

const getStepContent = (step, data, cartItems) => {
  switch (step) {
    case 0:
      return <CheckoutInfo data={data} cartItems={cartItems} />;
    case 1:
      return <PaymentInfo />;
    case 3:
      return <ConfirmOrder data={data} cartItems={cartItems}/>;
    default:
      return null;
  }
};

// Define your components for each step
const CheckoutInfo = ({data,cartItems}) =>{
  const id = localStorage.getItem("id");
 // console.log("id in addrs= " + id);
  const shippingCharge = 50;
  const navigate = useNavigate();
  const [addresses, setAddresses] = useState([]);
  const [selectedAddress, setSelectedAddress] = useState(''); 
 
  useEffect(() => {
    fetchAddresses();
  }, []);

  const fetchAddresses = async () => {
    try {
      const response = await userService.getAddress(id);
      if (!response.ok) {
        throw new Error('Failed to fetch addresses');
      }
      const data = await response.json();
      const dat=data.addresses;
      setAddresses(data.addresses);
    } catch (error) {
      console.error(error.message);
    }
  };

  const handleAddressChange = (event) => {
    setSelectedAddress(event.target.value);
    const addressId=event.target.value;
    localStorage.setItem("addressId",addressId);
  };

  const handleNewAddress = () => {
    navigate("/user/userheader/useraddaddress");
  };

  const defaultValue = addresses.length > 0 ? addresses[0]._id.toString() : '';


 return(
  <><Grid container spacing={2} item xs={12}>
    
      <Item>Shipping Address
    
     <Divider/>
        
          <Box marginLeft={10}>
          <Addresscomponent addresses={addresses} />
            <Button variant="outlined" onClick={handleNewAddress}>Add New Address</Button>
          </Box>
       
     
      
       
    </Item>
  
  
  </Grid></>
)}


const PaymentInfo = () => {
  const [address, setAddress] = useState([]);
  const [cartItems, setCartItems] = useState([]);
  const addressId=localStorage.getItem("addressId");
  const userId=localStorage.getItem("id");
  const shippingCharge=50;
  const vendorId="No Vendor";
  useEffect(() => {
    const fetchData = async () => {
    try {
      const response =await userService.getAddressByid(addressId)
      if (!response.ok) {
         throw new Error('Failed to fetch addresses');
       }
       const data = await response.json();
       //console.log("addrs"+data.address)
       setAddress(data.address);

       const res =await cartService.getCart(userId,vendorId);
       if (!res.ok) {
          throw new Error('Failed to fetch data');
        }
        const result1 = await res.json();
        //console.log("cart= "+result1.productName)
        setCartItems(result1);
    } catch (error) {
        console.error('Error fetching data:', error);
    }
};
fetchData();
}, []);
return(
  <div>
  <Typography> 
    <Grid container spacing={2} >
  <Grid item xs={6} >
    <Item sx={{ textAlign:'left'}}>Contact Details<br></br>
    {/* {address&& address.map(address => ( */}
        {/* <Box key={address.id} sx={{ display: 'flex', flexDirection: 'column', width: '100%', border: '1px solid #ccc', padding: '10px', margin: '10px' }}>
           */}
          <Typography >Name : {address.name}</Typography>
          <Typography><HomeOutlinedIcon/> : {address.addressLine1}</Typography>
          <Typography> {address.city}, {address.state} {address.postalCode}</Typography>
          <Typography><PublicOutlinedIcon/> : {address.country}</Typography>
          <Typography><ContactPhoneOutlinedIcon/>: {address.phoneNumber}</Typography>
      
    </Item>
  </Grid>
  <Grid item xs={6}>
  <Paper elevation={3} sx={{ p: 1}}>
       <Box 
        alignItems="center"
        // width={500}
        // marginLeft={30}
        >
     <List>
       {(cartItems)&&cartItems.map((item, index) => ( 
       <ListItem key={index} >
         <ListItemText
               primary={item.productName}
               secondary={`Quantity: ${item.quantity}`}
         />
     <Typography variant="body2">
       Rs.{(item.productPrice * item.quantity).toFixed(2)}
     </Typography>
   </ListItem>
  
  ))}  
 </List>
 <List>
    <ListItem  >
     <ListItemText
        primary='Shipping Charge'
      />
     <Typography variant="body2">
       Rs.{shippingCharge}
     </Typography>      
   </ListItem>
 </List>
 <Typography variant="h6" backgroundColor="lightblue">
     Total: Rs.{cartItems && cartItems
     .reduce((total, item) => total + shippingCharge + (item.productPrice * item.quantity), 0) // Calculate subtotal
     .toFixed(2)}
 </Typography><br></br>
 <Grid item xs={12} >
   <PaymentForm />
      </Grid>
      </Box>
        </Paper>
  </Grid>
  </Grid>
  </Typography>
  </div>
);
      }
const ConfirmOrder = () => (
  <Typography>
 
  </Typography>
);

export default Checkout;
