import React, { useState } from 'react';
import { Nav, Breadcrumb } from 'react-bootstrap';
import HomeRoundedIcon from '@mui/icons-material/HomeRounded';
import ChevronRightRoundedIcon from '@mui/icons-material/ChevronRightRounded';
import { Outlet, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

export default function Userheader() {
    const [title, setTitle] = useState('Profile');
    const navigate = useNavigate();
    
    const handleTabSelect = (selectedTab) => {
        switch (selectedTab) {
            case 'profile':
                setTitle('Profile');
                navigate('/user/userheader/userprofile');
                break;
            case 'orders':
                setTitle('My Orders');
                navigate('/user/userheader/userorders');
                break;
            case 'wishlist':
                setTitle('My Wishlist');
                navigate('/user/userheader/userwish');
                break;
            case 'address':
                setTitle('Your Addresses');
                navigate('/user/userheader/useraddress');
                break;
            case 'logout':
                localStorage.removeItem('id');
                localStorage.removeItem('addressId');
                navigate('/homepage');
                toast.success('Logout Successful!!!');
                break;
            default:
                setTitle('Profile');
                navigate('/user/userheader/userprofile');
        }
    };

    return (
        <div>
            <div
                style={{
                    position: 'sticky',
                    top: -100, 
                    backgroundColor: 'white',
                    zIndex: 9995,
                    paddingLeft: '16px', 
                    paddingRight: '16px', 
                }}
            >
                <Breadcrumb separator={<ChevronRightRoundedIcon fontSize="small" />}>
                    <Breadcrumb.Item>
                        <HomeRoundedIcon />
                    </Breadcrumb.Item>
                    <Breadcrumb.Item href="#" onClick={() => handleTabSelect('profile')}>
                        Users
                    </Breadcrumb.Item>
                    <Breadcrumb.Item active>{title}</Breadcrumb.Item>
                </Breadcrumb>
                <Nav fill variant="tabs" defaultActiveKey="profile" onSelect={handleTabSelect} style={{ width: '50%' }}>
                    <Nav.Item style={{ width: '10%' }}>
                        <Nav.Link eventKey="profile">My Profile</Nav.Link>
                    </Nav.Item>
                    <Nav.Item style={{ width: '10%' }}>
                        <Nav.Link eventKey="orders">My Orders</Nav.Link>
                    </Nav.Item>
                    <Nav.Item style={{ width: '10%' }}>
                        <Nav.Link eventKey="wishlist">Shop Now</Nav.Link>
                    </Nav.Item>
                    <Nav.Item style={{ width: '10%' }}>
                        <Nav.Link eventKey="address">My Address</Nav.Link>
                    </Nav.Item>
                    <Nav.Item style={{ width: '10%' }}>
                        <Nav.Link eventKey="logout">Logout</Nav.Link>
                    </Nav.Item>
                </Nav>
            </div>
            <Outlet />
        </div>
    );
}
