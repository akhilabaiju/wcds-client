import React, { useState, useEffect } from 'react';
import { Paper, Grid, Table, TableHead, TableBody, TableRow, TableCell } from '@mui/material';
import orderService from '../../services/orderService';
import manufacturerService from '../../services/manufacturerService';
import productService from '../../services/productService';

const UserBills = () => {
  const [data, setData] = useState(null);
  const [manufacturers, setManufacturers] = useState({});
  const [products, setProducts] = useState({});

  useEffect(() => {
    const fetchData = async () => {
      try {
        // Fetch orders
        const id = localStorage.getItem("id");
        const response = await orderService.getOrders(id);
        if (!response.ok) {
          throw new Error('Failed to fetch data');
        }
        const result = await response.json();
        setData(result);

        // Fetch manufacturers
        const manufacturerResponse = await manufacturerService.getAllManufacturers();
        if (!manufacturerResponse.ok) {
          throw new Error('Failed to fetch manufacturers');
        }
        const manufacturerData = await manufacturerResponse.json();
        const manufacturerMap = {};
        manufacturerData.forEach(manufacturer => {
          manufacturerMap[manufacturer._id] = manufacturer.firstName + " " + manufacturer.lastName;
        });
        setManufacturers(manufacturerMap);

        // Fetch products
        const productResponse = await productService.getAllProducts();
        if (!productResponse.ok) {
          throw new Error('Failed to fetch products');
        }
        const productsData = await productResponse.json();
        const productsMap = {};
        productsData.forEach(product => {
          productsMap[product._id] = {
            name: product.productName,
            weight: product.productWeight
          };
        });
        setProducts(productsMap);
      } catch (error) {
        console.error('Error fetching data:', error.message);
      }
    };

    fetchData();
  }, []);

  return (
    <div>
      <Grid item xs={12}>
        <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell>Product Name</TableCell>
                <TableCell>Product Weight(in Liters)</TableCell>
                <TableCell>Manufacture Name</TableCell>
                <TableCell>Product Price(Rs.)</TableCell>
                <TableCell>Quantity</TableCell>
                <TableCell>Amount Paid</TableCell>
                <TableCell>Date Of Order</TableCell>
                <TableCell>Delivery Date</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data && data.map(row => (
                <TableRow key={row._id}>
                  <TableCell>{products[row.productId]?.name}</TableCell>
                  <TableCell>{products[row.productId]?.weight}</TableCell>
                  <TableCell>{manufacturers[row.manufactureId]}</TableCell>
                  <TableCell>{row.amount}</TableCell>
                  <TableCell>{row.quantity}</TableCell>
                  <TableCell>{row.totalAmount}</TableCell>
                  <TableCell>{row.createdAt}</TableCell>
                  <TableCell>With in 48 hours</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Paper>
      </Grid>
    </div>
  );
};

export default UserBills;
