import React,{useState,useEffect} from 'react';
import { Box, Typography, Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import userService from '../../services/userService';
import HomeIcon from '@mui/icons-material/Home';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import PublicIcon from '@mui/icons-material/Public';
import PublicOutlinedIcon from '@mui/icons-material/PublicOutlined';
import ContactPhoneIcon from '@mui/icons-material/ContactPhone';
import ContactPhoneOutlinedIcon from '@mui/icons-material/ContactPhoneOutlined';
import { toast } from 'react-toastify';
function AddressPage() {
  const navigate = useNavigate();
  const [addresses, setAddresses] = useState([]);
  const userId=localStorage.getItem("id")

  useEffect(() => {
     fetchAddresses();
  }, []);

  const fetchAddresses = async () => {
    try {
       const response =await userService.getAddress(userId)
     if (!response.ok) {
        throw new Error('Failed to fetch addresses');
      }
      const data = await response.json();
      setAddresses(data.addresses);
      
    } catch (error) {
      console.error(error.message);
    }
  };
 
 

  // Function to handle editing an address
  const editAddress =async (id) => {
    try {
      const response = await userService.updateAddress(id);
  } catch (error) {
      console.error('Error updating user:', error);
  }
 
  toast.success("Address Updated Succseefully!!! ");
    
  };

  // Function to handle removing an address
  const removeAddress = async(id) => {
    await userService.removeAddress(id);
    const response =await userService.getAddress(userId)
    if (!response.ok) {
       throw new Error('Failed to fetch addresses');
     }
     const data = await response.json();
     setAddresses(data.addresses);
  };

  // Function to handle adding a new address
  const addAddress = () => {
  navigate("/user/userheader/useraddaddress")
  };
 
  return (
    <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', padding: '20px', width: '100%', flexWrap: 'wrap' }}>
      {addresses.map(address => (
        <Box key={address._id} sx={{ display: 'flex', flexDirection: 'column', width: '30%', border: '1px solid #ccc', padding: '10px', margin: '10px' }}>
          
          <Typography variant="h6">Name : {address.name}</Typography>
          <Typography><HomeOutlinedIcon/> : {address.addressLine1}</Typography>
          <Typography> {address.city}, {address.state} {address.postalCode}</Typography>
          <Typography><PublicOutlinedIcon/> : {address.country}</Typography>
          <Typography><ContactPhoneOutlinedIcon/>: {address.phoneNumber}</Typography>
          <Box sx={{ display: 'flex', justifyContent: 'space-between', marginTop: '10px' }}>
            {/* <Button variant="outlined" size="small" onClick={() => editAddress(address._id)}>Edit</Button> */}
            <Button variant="outlined" size="small" onClick={() => removeAddress(address._id)}>Remove</Button>
            {/* {!address.isDefault && (
              <Button variant="outlined" size="small" onClick={() => setAsDefault(address._id)}>Set as Default</Button>
            )} */}
          </Box>
        </Box>
      ))}
      <Button variant="contained" onClick={addAddress}>Add New Address</Button>
    </Box>
  );
}

export default AddressPage;
