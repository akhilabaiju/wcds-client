import React from 'react'
import  { useState, useEffect } from 'react';
import {
  List,
  ListItem,
  ListItemText,
  Typography,
  Divider,
  Link,
  Paper,
  Modal,
  Button,
  FormControl,
    Box,Grid,Card, CardMedia, CardContent, CardActions
} from "@mui/material";
import AppBar from '@mui/material/AppBar';
import InputLabel from '@mui/material/InputLabel';
import { styled } from '@mui/material/styles';
import NativeSelect from '@mui/material/NativeSelect';
import cartService from '../../services/cartService';
import userService from '../../services/userService'
import vendorService from '../../services/vendorService';
import { useNavigate } from 'react-router-dom';

const API_URL = process.env.REACT_APP_IMG_URL;
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  // padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));
const Vendorproducts = () => {
  const [vendors, setVendors] = useState(null);
  const [data, setData] = useState(null);
  const [cartItems, setCartItems] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [itemToDelete, setItemToDelete] = useState(null);
  const [selectedQuantities, setSelectedQuantities] = useState({});
  const [productData, setProductData] = useState({});
  const [vendorId,setVendorId] = useState(null)
  const navigate = useNavigate();
  const userId= localStorage.getItem("id");
 
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await userService.getVendor();
        if (!response.ok) {
          throw new Error('Failed to fetch data');
        }
        const result = await response.json();
       // console.log("res "+result)
        setVendors(result);
              
      
      } catch (error) {
        console.error('Error fetching data:', error.message);
      }

    };
    fetchData();
  }, [userId]);

  const handleVendor = async (event) => {
    const vendor_id = event.target.value;
    setVendorId(event.target.value)
    localStorage.setItem("vendorId",vendor_id);
    try {
        const response = await vendorService.getProductsbyId(vendor_id);
    if (!response.ok) {
      throw new Error('Failed to fetch data');
      setData([]);
    }        
    const result = await response.json();
    if (!result || result.length === 0) { setData([]);}
    else{ setData(result);}
     
    //************************************* */

     const res = await cartService.getCart(userId,vendor_id);
        if (!res.ok) {
              throw new Error('Failed to fetch data');
            }
            const carts = await res.json();
            setCartItems(carts);

    const productPromises = carts.map(item => getProductData(item.productId,vendor_id));
     const products = await Promise.all(productPromises);
   
   const productDataObject = Object.fromEntries(products.map((product, index) => [result[index].productId, product]));
   setProductData(productDataObject);
   
} catch (error) {
console.error('Error fetching data:', error.message);
setData([]);
}
} 

const getProductData = async (productId,vendorId) => {
  try {
    const response = await vendorService.getQuantityById(productId,vendorId);
    if (!response.ok) {
      throw new Error('Failed to fetch product data');
    }
    const productData = await response.json();
    console.log("productData= "+productData)
   const qnty= productData[0].pNumber;
  //const qnty = productData.pNumber;
   if (!qnty || qnty.length === 0) { return qnty}
    else{ return 0}
   // return productData[0].pNumber;
  } catch (error) {
    console.error('Error fetching product data:', error);
  }
};


const style = {
  position: 'absolute' ,
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  pt: 2,
  px: 4,
  pb: 3,
};
const handleOpenModal = (item) => {
setShowModal(true);
setItemToDelete(item);
};

const handleCloseModal = () => {
setShowModal(false);
setItemToDelete(null);
};

const handleConfirmDelete = () => {
if (itemToDelete) {
deleteItem(itemToDelete);
handleCloseModal();
}
};

const toCheckout = () => {
navigate('/user/userheader/usercheckout1');
};

  const getItemQuantityInCart = async (productId) => {
    try {
      const response = await cartService.getItemQuantityInCart(userId, productId,vendorId);
      if (!response.ok) {
        throw new Error('Failed to fetch quantity');
      }
      const data = await response.json();
      return data.quantity;
    } catch (error) {
      console.error('Error getting item quantity in cart:', error);
      return 0; // Return 0 if there's an error
    }
  };

  const addCart = async (e, product) => {
    e.preventDefault();
    try {
      const productId = product.productId;
      const quantityInCart = await getItemQuantityInCart(productId);
      const productQuantity = await getProductData(productId, vendorId);
      console.log("quantityInCart- " + quantityInCart);
      console.log("productQuantity- " + productQuantity);
      if (productQuantity > quantityInCart || quantityInCart === 0) {
        if (quantityInCart === 0) {
          cartService.addToCart({
            manufactureId: product.manufactureId,
            userId: userId,
            vendorId: vendorId,
            productId: product.productId,
            quantity: 1,
            productName: product.productName,
            productWeight: product.productWeight,
            productPrice: product.productPrice,
            imageUrl: product.imageUrl
          });
        } else {
          cartService.updateCart({
            manufactureId: product.manufactureId,
            userId: userId,
            vendorId: vendorId,
            productId: product.productId,
            quantity: quantityInCart + 1,
          });
        }
        const response = await cartService.getCart(userId, vendorId);
        if (!response.ok) {
          throw new Error('Failed to fetch data');
        }
        const result = await response.json();
        setCartItems(result);
      }
  
    } catch (error) {
      console.error('Error submitting form:', error);
      setCartItems([]);
    }
  }
  
  const deleteItem = async(item) => {
      await cartService.removeItem(item._id);
      const response = await cartService.getCart(userId,vendorId);
     if (!response.ok) {
               throw new Error('Failed to fetch data');
             }
             const result = await response.json();
             setCartItems(result);
  };

  const handleQuantityChange = async(event, item) => {
    try {
      // Get the new quantity from the event target value
      const newQuantity = event.target.value;
      // Update quantity in the database
      await cartService.updateCart({
        manufactureId: item.manufactureId,
        userId: userId,
        vendorId:vendorId,
        productId: item.productId,
        quantity: newQuantity
      });
      const response = await cartService.getCart(userId,vendorId);
      if (!response.ok) {
                throw new Error('Failed to fetch data');
              }
              const result = await response.json();
              setCartItems(result);
    } catch (error) {
      console.error('Error updating quantity:', error);
    }
  };
  

  return (

   
    <Box sx={{ flexGrow: 1, p: 2 }}>
       <AppBar position="static"  color="default">

       <Box component="form" sx={{'& .MuiTextField-root': { m: 2, width: '25ch', },}} noValidate autoComplete="off" >
        <Grid container spacing={2}>
        <Grid item xs={12}>
            <Item>
                <FormControl variant="standard" sx={{ m: 1, minWidth: 580 }}>
                <InputLabel htmlFor="vendors-available">Select Vendor</InputLabel>
                <NativeSelect defaultValue="" onChange={handleVendor} inputProps={{ id: 'vendors-available' }}>
                    <option value=""></option>
                      {vendors && vendors.map(row => (
                          <option key={row._id} value={row._id}>{row.firstName} {row.lastName}</option>
                      ))}
                </NativeSelect>
                </FormControl>
            </Item>
            </Grid>
        </Grid>
        </Box>
       </AppBar><br></br>
        <Grid container spacing={2}>
        <Grid item xs={12} md={8}>
          <Grid container spacing={2}>
          {(data && data.length > 0)?( data.map((product) => (
              <Grid item key={product._id} xs={12} sm={6} md={4}>
                <Card sx={{ maxWidth: 245 }}>
                  <CardMedia
                    component="img"
                    height="240"
                    image={`${API_URL}/` + product.imageUrl.replace(/src\\public\\/, '')}
                   // image={'http://localhost:3001//' + product.imageUrl.replace(/src\\public\\/, '')}
                    alt={product.name}
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      {product.productName}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                     
                        {product.pNumber >= 1 ? `Number of Available Items: ${product.pNumber}` : 'Currently out of stock'}
                    </Typography>
                  </CardContent>
                  <CardActions>
                  {product.pNumber >= 1 ? <Button size="small" onClick={(e) => addCart(e, product)}>
                      Add to Cart
                  </Button> : <Button size="small" disabled>
                      Add to Cart
                  </Button> }
                 </CardActions>
                </Card>
              </Grid>
            ))
          ) : (
            <Grid item xs={12}>
            <Typography>No products available for this vendor</Typography>
        </Grid>
          )}
          </Grid>
        </Grid> 
        <Grid item xs={12} md={4}>
        <Paper elevation={3} sx={{ p: 2 }}>
      <Typography variant="h6" gutterBottom>
        Shopping Cart
      </Typography>
      <Divider />
      <List>
        {cartItems.map((item, index) => (
          <ListItem key={index} divider>
            <ListItemText
              primary={item.productName}
              
      secondary={`Quantity: ${item.quantity}`} 
    />
            <Typography variant="body2">
              Rs.{(item.productPrice * item.quantity).toFixed(2)}<br></br>
              <Link
                component="button"
                variant="body2"
                underline='none'
                onClick={() => handleOpenModal(item)}
              >
                Delete
              </Link>
            </Typography>
          </ListItem>
        ))}
      </List>
      <Typography variant="h6">
        Total: Rs.
        {cartItems
          .reduce((total, item) => total + item.productPrice * item.quantity, 0)
          .toFixed(2)}
      </Typography>

      <Modal open={showModal} onClose={handleCloseModal}>
        <div>
          <Box sx={{ ...style }}>
            <Typography variant="h6">Confirm Deletion</Typography>
            <Typography variant="body1">
              Are you sure you want to delete this item?
            </Typography>
            <Button onClick={handleConfirmDelete}>Delete</Button>
            <Button onClick={handleCloseModal}>Cancel</Button>
          </Box>
        </div>
      </Modal>

      {cartItems.length > 0 && (
        <Button fullWidth variant="contained" sx={{ mt: 1, mb: 2 }} onClick={toCheckout}>
          Proceed to Checkout
        </Button>
      )}
    </Paper>
        </Grid>
      </Grid> 
    </Box>
  )
}


export default Vendorproducts

