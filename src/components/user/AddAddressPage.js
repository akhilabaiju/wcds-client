import React from 'react';
import { Box, Typography, TextField, Button } from '@mui/material';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import userService from '../../services/userService';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate } from 'react-router-dom';
const AddAddressPage = () => {
  const navigate = useNavigate();
 // const userId=localStorage.getItem("id");
  // Define validation schema using Yup
  const validationSchema = Yup.object({
    name: Yup.string().required('Name is required'),
    addressLine1: Yup.string().required('Address Line 1 is required'),
    city: Yup.string().required('City is required'),
    state: Yup.string().required('State is required'),
    postalCode: Yup.string().required('Postal Code is required'),
    country: Yup.string().required('Country is required'),
   // phoneNumber: Yup.string().required('Phone Number is required'),
    phoneNumber: Yup.string().required("required"),
    phoneNumber: Yup.string()
    .matches(/^[0-9]+$/, 'Mobile number should only contain digits')
    .matches(/^((?![eE]).)*$/, 'Letter "e" is not allowed')
    .min(10, 'Mobile number must be at least 10 digits')
    .max(12, 'Mobile number must be at maximun 12 digits')
    .required('Mobile number is required'),
  });

  // Initial form values
  const initialValues = {
    userId:localStorage.getItem("id"),
    name: '',
    addressLine1: '',
    addressLine2: '',
    city: '',
    state: '',
    postalCode: '',
    country: '',
    phoneNumber: '',
  };

  // Function to handle form submission
  const handleSubmit = (values, { resetForm }) => {
    try {
      const response = userService.addAddress(values);
    } catch (error) {
      console.error('Error submitting form:', error);
    }
    toast.success("Address Added Successfully");
    navigate('/user/userheader/useraddress');
    resetForm();
  };

  return (
    <Box sx={{ padding: '20px', width: '100%', marginLeft: '30%', height: '80%' }}>
      <Typography variant="h6" gutterBottom>Add Address</Typography>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ errors, touched }) => (
          <Form>
            <Box sx={{ display: 'flex', flexDirection: 'column', gap: '15px', maxWidth: '400px' }}>
              <Field name="name" as={TextField} label="Name" error={touched.name && !!errors.name} helperText={touched.name && errors.name} size="small" />
              <Field name="addressLine1" as={TextField} label="Address Line 1" error={touched.addressLine1 && !!errors.addressLine1} helperText={touched.addressLine1 && errors.addressLine1} size="small" />
              <Field name="addressLine2" as={TextField} label="Address Line 2" size="small" />
              <Field name="city" as={TextField} label="City" error={touched.city && !!errors.city} helperText={touched.city && errors.city} size="small" />
              <Field name="state" as={TextField} label="State" error={touched.state && !!errors.state} helperText={touched.state && errors.state} size="small" />
              <Field name="postalCode" as={TextField} label="Postal Code" error={touched.postalCode && !!errors.postalCode} helperText={touched.postalCode && errors.postalCode} size="small" />
              <Field name="country" as={TextField} label="Country" error={touched.country && !!errors.country} helperText={touched.country && errors.country} size="small" />
              <Field name="phoneNumber" as={TextField} label="Phone Number" error={touched.phoneNumber && !!errors.phoneNumber} helperText={touched.phoneNumber && errors.phoneNumber} size="small" />
              <Button type="submit" variant="contained">Submit</Button>
            </Box>
          </Form>
        )}
      </Formik>
    </Box>
  );
};

export default AddAddressPage;
