import React, { useEffect, useState } from 'react';
import userService from '../../services/userService';
import UpdateForm from './UpdateForm';
import { Card, Container, Row, Col, Button } from 'react-bootstrap';

export default function Profile() {
  const [data, setData] = useState(null);
  const id = localStorage.getItem('id');
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    email: '',
    contactNumber: '',
    //address: '',
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await userService.getUserById(id);
        const result = await response.json();
        setData(result);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, []);

  const handleUpdate = async (values) => {
    alert("update");
  };

  return (
    <Container>
      <Row className="justify-content-center">
        <Col md={8}>
          <Card>
            <Card.Body>
              <Card.Title>Personal info</Card.Title>
              <hr />
              <Row>
             
                <Col md={8}>
                  {data && <UpdateForm initialValues={data} />}
                </Col>
              </Row>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}