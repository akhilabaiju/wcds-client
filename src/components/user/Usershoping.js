import React from 'react';
import { useState, useEffect } from 'react';
import productService from '../../services/productService';
import { Grid, Box } from '@mui/material';
import { Card, CardMedia, CardContent, CardActions, Typography, Button,Paper,Divider,List,ListItem,ListItemText,Link,Modal } from '@mui/material';
import cartService from '../../services/cartService';
import CartPage from '../cart/CartPage';
import BlinkingMessage from './BlinkingMessage';
import { useNavigate } from 'react-router-dom';

const API_URL = process.env.REACT_APP_IMG_URL;

const Usershoping = () => {
  const [data, setData] = useState(null);
  const [cartItems, setCartItems] = useState([]);
  const userId = localStorage.getItem("id");
  const vendorId = "No Vendor";
  const [showModal, setShowModal] = useState(false);
  const [itemToDelete, setItemToDelete] = useState(null);
  const [selectedQuantities, setSelectedQuantities] = useState({});
  const [productData, setProductData] = useState({});
  const navigate = useNavigate();
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await productService.getAllProducts();
        if (!response.ok) {
          throw new Error('Failed to fetch data');
        }
        const result = await response.json();

        setData(result);
        const res = await cartService.getCart(userId, vendorId);
        if (!res.ok) {
          throw new Error('Failed to fetch data');
        }
        const carts = await res.json();
        setCartItems(carts);
      } catch (error) {
        console.error('Error fetching data:', error.message);
      }
    };
    fetchData();
  }, [userId]);

  const getItemQuantityInCart = async (productId) => {
    try {
      const response = await cartService.getItemQuantityInCart(userId, productId, vendorId);
      if (!response.ok) {
        throw new Error('Failed to fetch quantity');
      }
      const data = await response.json();
      return data.quantity;
    } catch (error) {
      console.error('Error getting item quantity in cart:', error);
      return 0; // Return 0 if there's an error
    }
  };

  const style = {
    position: 'absolute' ,
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    pt: 2,
    px: 4,
    pb: 3,
  };
  const addCart = async (e, product) => {
    e.preventDefault();
    try {
      const productId = product._id;
      const quantityInCart = await getItemQuantityInCart(productId, vendorId);
      if (quantityInCart === 0) {
        cartService.addToCart({
          manufactureId: product.manufactureId,
          userId: userId,
          vendorId: vendorId,
          productId: product._id,
          quantity: 1,
          productName: product.productName,
          productWeight: product.productWeight,
          productPrice: product.productPrice,
          imageUrl: product.imageUrl
        });
      } else {
        cartService.updateCart({
          manufactureId: product.manufactureId,
          userId: userId,
          vendorId: vendorId,
          productId: product._id,
          quantity: quantityInCart + 1,
        });
      }
      const response = await cartService.getCart(userId, vendorId);
      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      const result = await response.json();
      setCartItems(result);
    } catch (error) {
      console.error('Error submitting form:', error);
    }
  };

  const deleteItem = async (item) => {
    await cartService.removeItem(item._id);
    const response = await cartService.getCart(userId, vendorId);
    if (!response.ok) {
      throw new Error('Failed to fetch data');
    }
    const result = await response.json();
    setCartItems(result);
  };

  const handleQuantityChange = async (event, item) => {
    try {
      // Get the new quantity from the event target value
      const newQuantity = event.target.value;
      // Update quantity in the database
      await cartService.updateCart({
        manufactureId: item.manufactureId,
        userId: userId,
        vendorId: vendorId,
        productId: item.productId,
        quantity: newQuantity
      });
      const response = await cartService.getCart(userId, vendorId);
      if (!response.ok) {
        throw new Error('Failed to fetch data');
      }
      const result = await response.json();
      setCartItems(result);
    } catch (error) {
      console.error('Error updating quantity:', error);
    }
  };

  const handleOpenModal = (item) => {
    setShowModal(true);
    setItemToDelete(item);
  };

  const handleCloseModal = () => {
    setShowModal(false);
    setItemToDelete(null);
  };

  const handleConfirmDelete = () => {
    if (itemToDelete) {
      deleteItem(itemToDelete);
      handleCloseModal();
    }
  };

  const toCheckout = () => {
    navigate('/user/userheader/usercheckout');
  };
  return (
    <Box sx={{ flexGrow: 1, p: 2 }}>
      <BlinkingMessage />
      <Grid container spacing={2}>
        <Grid item xs={12} md={8}>
          <Grid container spacing={2}>
            {(data) && data.map((product) => (
              <Grid item key={product._id} xs={12} sm={6} md={4}>
                <Card sx={{ maxWidth: 245 }}>
                  <CardMedia
                    component="img"
                    height="240"
                     image={`${API_URL}/` + product.imageUrl.replace(/src\\public\\/, '')}
                   // image={'http://localhost:3001//' + product.imageUrl.replace(/src\\public\\/, '')}
                    alt={product.name}
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      {product.productName}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      {product.pNumber >= 1 ? `Number of Available Items: ${product.pNumber}` : 'Currently out of stock'}
                    </Typography>
                  </CardContent>
                  <CardActions>
                    {product.pNumber >= 1 ? <Button size="small" onClick={(e) => addCart(e, product)}>
                      Add to Cart
                    </Button> : <Button size="small" disabled>
                      Add to Cart
                    </Button>}
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Grid>
        {/* <Grid item xs={12} md={4}>
          <CartPage
            cartItems={cartItems}
            handleQuantityChange={handleQuantityChange}
            deleteItem={deleteItem}
          />
        </Grid> */}
        <Grid item xs={12} md={4}>
        <Paper elevation={3} sx={{ p: 2 }}>
      <Typography variant="h6" gutterBottom>
        Shopping Cart
      </Typography>
      <Divider />
      <List>
        {cartItems.map((item, index) => (
          <ListItem key={index} divider>
            <ListItemText
              primary={item.productName}
              
      secondary={`Quantity: ${item.quantity}`} 
    />
            <Typography variant="body2">
              Rs.{(item.productPrice * item.quantity).toFixed(2)}<br></br>
              <Link
                component="button"
                variant="body2"
                underline='none'
                onClick={() => handleOpenModal(item)}
              >
                Delete
              </Link>
            </Typography>
          </ListItem>
        ))}
      </List>
      <Typography variant="h6">
        Total: Rs.
        {cartItems
          .reduce((total, item) => total + item.productPrice * item.quantity, 0)
          .toFixed(2)}
      </Typography>

      <Modal open={showModal} onClose={handleCloseModal}>
        <div>
          <Box sx={{ ...style }}>
            <Typography variant="h6">Confirm Deletion</Typography>
            <Typography variant="body1">
              Are you sure you want to delete this item?
            </Typography>
            <Button onClick={handleConfirmDelete}>Delete</Button>
            <Button onClick={handleCloseModal}>Cancel</Button>
          </Box>
        </div>
      </Modal>

      {cartItems.length > 0 && (
        <Button fullWidth variant="contained" sx={{ mt: 1, mb: 2 }} onClick={toCheckout}>
          Proceed to Checkout
        </Button>
      )}
    </Paper>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Usershoping;
