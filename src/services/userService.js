import axios from 'axios';
const API_URL = process.env.REACT_APP_API_URL;
const USER_API_URL = `${API_URL}/user`;

const userService =  {
    getUserById:async(id)=>{
        try{
             const response =await fetch(`${USER_API_URL}/byid/${id}`);
            return response
            }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      },

      getAllUsers:async()=>{
        try{
            const response =await fetch(`${USER_API_URL}/getAllUsers`);
            return response
          
        }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      },

      getVendor:async()=>{
        try{ 
            const response =await fetch(`${USER_API_URL}/getVendors`);
            return response
          
        }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      },

      getManufacturer:async(id)=>{
        try{
          //console.log("in service")
          const response =await fetch(`${USER_API_URL}/user/manufacturer/${id}`);
          return response
     }catch(error){
       console.error('Error fetching user:', error);
       throw error;
     }
   },


   sendMail:async(id)=>{
    try{
      //console.log("in service")
      const response =await axios.post(`${USER_API_URL}/sendMail`,id);
      return response
 }catch(error){
   console.error('Error fetching user:', error);
   throw error;
 }
},

updateUser:async(values)=>{
  try{
    const response =await axios.put(`${USER_API_URL}/update`,values);
   return response;
}catch(error){
  console.error('Error fetching user:', error);
  throw error;
}
},

addAddress:async(values)=>{
  try{
    const response =await axios.post(`${USER_API_URL}/addresses`,values);
   return response;
}catch(error){
  console.error('Error fetching user:', error);
  throw error;
}
},

getAddress:async(id)=>{
  try{
    const response =await fetch(`${USER_API_URL}/addresses/${id}`);
    return response
}catch(error){
 console.error('Error fetching user:', error);
 throw error;
}
},

getAddressByid:async(id)=>{
  try{
   // console.log("in service"+id)
    const response =await fetch(`${USER_API_URL}/addresses/addressbyid/${id}`);
    return response
}catch(error){
 console.error('Error fetching user:', error);
 throw error;
}
},

removeAddress:async(id)=>{
  try{
    const response = axios.put(`${USER_API_URL}/removeaddress/${id}`);
    return response;
  }catch(error){
    console.error('Error fetching user:', error);
    throw error;
  }
}
}
export default userService