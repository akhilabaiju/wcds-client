import axios from 'axios';
const API_URL = process.env.REACT_APP_API_URL;
const ORDER_API_URL = `${API_URL}/orders`;

const orderService = {
 
  placeOrder:async(data)=>{
    try{
   
      const response = axios.post(`${ORDER_API_URL}/placeorder`,data);
      return response;
    }catch(error){
      console.error('Error fetching user:', error);
      throw error;
    }
  },

    getOrders:async(id)=>{
    try{
       
         const response =await fetch(`${ORDER_API_URL}/orders/${id}`);
        return response
      
    }catch(error){
      console.error('Error fetching user:', error);
      throw error;
    }
  },

  getManufactureOrders:async(id)=>{
    try{
       
         const response =await fetch(`${ORDER_API_URL}/getManufactureorders/${id}`);
        return response
      
    }catch(error){
      console.error('Error fetching user:', error);
      throw error;
    }
  },
  placeOrderV:async(data)=>{
    try{
   
      const response = axios.post(`${ORDER_API_URL}/placeorderv`,data);
      return response;
    }catch(error){
      console.error('Error fetching user:', error);
      throw error;
    }
  },
  

}

export default orderService