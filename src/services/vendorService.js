
import axios from 'axios';

const API_URL = process.env.REACT_APP_API_URL;
const VENDOR_API_URL = `${API_URL}/vendor`;

const vendorService = {
           
    getProductsbyId:async(id)=>{
        try{
           
             const response =await fetch(`${VENDOR_API_URL}/productsbyvendor/${id}`);
             return response
          
        }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      }, 
      getQuantityById:async(id,vendorId)=>{
        try{
           
             const response =await fetch(`${VENDOR_API_URL}/getquantitybyid/${id}/${vendorId}`);
               return response
          
        }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      },

      getItemQuantityInCart:async(userId, productId,vendorId)=>{
        try{ 
         const response = await fetch(`${VENDOR_API_URL}/getquantity/${userId}/${productId}/${vendorId}`);
            return response
            }catch(error){
          console.error('Error fetching user:', error); 
          throw error;
        }
      },
      getOrders:async(id)=>{
        try{
           
             const response =await fetch(`${VENDOR_API_URL}/orders/${id}`);
            return response
          
        }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      },

      placeOrder:async(data)=>{
        try{
       
          const response = axios.post(`${VENDOR_API_URL}/placeorder`,data);
          return response;
        }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      },
   
}

export default vendorService