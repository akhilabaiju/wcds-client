
import axios from 'axios';
const API_URL = process.env.REACT_APP_API_URL;
const PRO_API_URL = `${API_URL}/products`;

const productService = {
       addProducts:(formData)=>{
        try{  
       
                  const response = axios.post(`${PRO_API_URL}/add`,formData);
                  return response;
           }
        catch(error){
                  console.error('Error fetching user:', error);
                  throw error;
                }
      }, 

      uploadProducts:(formData)=>{
        try{  
        
          const response = axios.post(`${PRO_API_URL}/add`,formData,{
       
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          });
          return response;
           }
        catch(error){
                  console.error('Error fetching user:', error);
                  throw error;
                }
      },

     
      getProducts:async(id)=>{
        try{
           
             const response =await fetch(`${PRO_API_URL}/byuserid/${id}`);
            return response
          
        }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      },
      getpNumberById:async(id)=>{
        try{
           
             const response =await fetch(`${PRO_API_URL}/pnumberbyid/${id}`);
            return response
          
        }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      },
      getAllProducts:async()=>{
        try{
      
            const response =await fetch(`${PRO_API_URL}/getAllProducts`);
            return response
          
        }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      },

      getProductsById:async(id)=>{
        try{
           
             const response =await fetch(`${PRO_API_URL}/byproductid/${id}`);
            // console.log("respo in service= "+response.productName)
            return response
          
        }catch(error){
          console.error('Error fetching user:', error);
          throw error;
        }
      },
      updateProduct:async(values)=>{
        try{  
          // console.log("in service= "+formData.photo)
                   const response = axios.put(`${PRO_API_URL}/update`,values);
                   return response;
            }
         catch(error){
                   console.error('Error fetching user:', error);
                   throw error;
                 }
      },


}

export default productService