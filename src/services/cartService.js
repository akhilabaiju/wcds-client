import axios from 'axios';
const API_URL = process.env.REACT_APP_API_URL;
const CART_API_URL = `${API_URL}/cart`;

const cartService = {
  addToCart:async(formData)=>{
    try{
      const response = axios.post(`${CART_API_URL}/addcart`,formData);
      return response;
    }catch(error){
      console.error('Error fetching user:', error);
      throw error;
    }
  },
    
    getItemQuantityInCart:async(userId, productId,vendorId)=>{
        try{ 
         const response = await fetch(`${CART_API_URL}/getquantity/${userId}/${productId}/${vendorId}`);
        
         return response
            }catch(error){
          console.error('Error fetching user:', error); 
          throw error;
        }
      },
   

      updateCart:async(formData)=>{
       // console.log("form = "+formData)
    try{
      const response = axios.put(`${CART_API_URL}/updateCart`,formData);
      return response;
    }catch(error){
      console.error('Error fetching user:', error);
      throw error;
    }
  },

  getCart:async(id,vendorId)=>{
    try{
       
         const response =await fetch(`${CART_API_URL}/cart/${id}/${vendorId}`);
        return response
      
    }catch(error){
      console.error('Error fetching user:', error);
      throw error;
    }
  },

  
  removeItem:async(id,vendorId)=>{
    try{
      const response = axios.put(`${CART_API_URL}/remove/${id}/${vendorId}`);
      return response;
    }catch(error){
      console.error('Error fetching user:', error);
      throw error;
    }
  },

  

}

export default cartService