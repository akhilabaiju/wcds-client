
import axios from 'axios';
const API_URL = process.env.REACT_APP_API_URL;
const ACC_API_URL = `${API_URL}/accounts`;
 

const loginService = {
       signupUser:(formData)=>{
        try{ 
                  const response = axios.post(`${ACC_API_URL}/signup`,formData);
                  return response;
          }
        catch(error){
                  console.error('Error fetching user:', error);
                  throw error;
                }
      },

      loginUser:async(formData)=>{
        try{       
                  const response =await axios.post(`${ACC_API_URL}/login/`,formData);
                  const id=response.data.userId;
                  const role=response.data.userType;
                  if (role === "customer") {
                    localStorage.setItem("id", id); // Set user ID in localStorage
                    return "/user/userheader"; // Redirect to "/user/userheader"
                  } else {
                    localStorage.setItem("id", id); // Set user ID in localStorage
                    return "/" + role; // Redirect to a route based on the user's role
                  }
                  
             }
        catch(error){
                  console.error('Error fetching user:', error);
                  throw error;
                }
      },
 

      resetPassword:async(formData)=>{
        try{ 
                  const response =await axios.post(`${ACC_API_URL}/resetpassword`,formData);
                  return response;
             
           }
        catch(error){
                  console.error('Error fetching user:', error);
                  throw error;
                }
      }
}

export default loginService