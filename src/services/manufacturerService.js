const API_URL = process.env.REACT_APP_API_URL;
const MANUFACTURER_API_URL = `${API_URL}/manufacturer`;

const manufacturerService =  {

      getManufacturer:async(id)=>{
        try{
         
          const response =await fetch(`${MANUFACTURER_API_URL}/manufacturer/${id}`)
          return response
     }catch(error){
       console.error('Error fetching user:', error);
       throw error;
     }
   },
   getAllManufacturers:async()=>{
    try{
    
        const response =await fetch(`${MANUFACTURER_API_URL}/allmanufacturers`);
        return response
      
    }catch(error){
      console.error('Error fetching user:', error);
      throw error;
    }
  },



}


export default manufacturerService