import axios from 'axios';

const API_URL = process.env.REACT_APP_API_URL;
const MAIL_API_URL = `${API_URL}/mail`;

const mailService =  {
sendMail:async(id)=>{
    try{
    
      const response =await axios.post(`${MAIL_API_URL}/sendMail`,id);
      return response
 }catch(error){
   console.error('Error fetching user:', error);
   throw error;
 }
}
}
export default mailService