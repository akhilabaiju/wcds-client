import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import Home from '../components/home/Home.js';
import Login from '../components/login/Login.js';
import SignUp from '../components/login/Signup.js';
import Welcome from '../components/login/Welcome.js';
import Aboutus from '../components/home/Aboutus.js';
import Forgetpassword from '../components/login/Forgetpassword.js';
import Resetpassword from '../components/login/Resetpassword.js';

//import User from '../components/user/Userdashboard.js';
import Userheader from '../components/user/Userheader.js';
import UserProfile from '../components/user/Profile.js';
import UserOrders from '../components/user/Userorders.js';
import UserBills from '../components/user/UserBills.js';
import UserWish from '../components/user/Usershoping.js';
import UserAddress from '../components/user/AddressPage.js';
import UserAddAddress from '../components/user/AddAddressPage.js';
import VendorShopProducts from '../components/user/Vendorproducts.js';
import ProductsList from '../components/products/Products.js';

import Footer from '../components/home/Footer.js';
import VendorCheckout from '../components/cart/Checkout1.js';
import Homepage from '../components/home/Homepage.js';
import Checkout from '../components/cart/Checkout.js';

import VendorHome from '../components/vendor/Home.js';
import VendorDashboard from '../components/vendor/Dashboard.js';
import VendorAddProducts from '../components/vendor/Addproducts.js';
import VendorAddproductform from '../components/vendor/Addproductform.js';
import VendorProducts from '../components/vendor/Myproducts.js';
import VendorOrders from '../components/vendor/Orders.js';

import ManufactureDashboard from '../components/manufacturer/Dashboard.js';
import ManufactureAddProducts from '../components/manufacturer/Addproducts.js';
import ManufactureListProducts from '../components/manufacturer/Listproducts.js';
import ViewOrders from '../components/manufacturer/Orders.js'
import ManufactureIssues from '../components/manufacturer/Issues.js';
import ManufactureUpdateProduct from '../components/manufacturer/Updateproduct.js';

const MainRoutes = () => {
  const BASE_PATH = '/';
  const PATH_LOGIN = '/login';
  const PATH_SIGNUP = '/signup';
  const PATH_HOME = "/homepage";
 const PATH_WELCOME ="/welcome";
 const PATH_ABOUTUS ="/aboutus";
 const PATH_FORGETPASSWORD ="/forgetpassword";
 const PATH_RESETPASSWORD ="/resetpassword";

 const PATH_PRODUCTS = "/productslist";

 const PATH_FOOTER="/footer";
 

//const PATH_USER = "/user";
const PATH_USERHEADER = "/user/userheader"
const PATH_USERPROFILE = "/user/userheader/userprofile";
const PATH_USERADDRESS = "/user/userheader/useraddress";
const PATH_USERADDADDRESS = "/user/userheader/useraddaddress";
const PATH_USERBILLS = "/user/userheader/userbills";
const PATH_USERORDERS = "/user/userheader/userorders";
const PATH_USERWISH = "/user/userheader/userwish";
const PATH_USER_CHECKOUT ="/user/userheader/usercheckout";
const PATH_VEN_SHOP_PRO = "/user/userheader/shopproducts";
const PATH_VEN_CHECKOUT = "/user/userheader/usercheckout1";

const PATH_VEN_DASH = "/vendor";
const PATH_VEN_HOME = "/vendor/home";
const PATH_VEN_ADD = "/vendor/addproducts";
const PATH_VEN_PRO = "/vendor/myproducts";
const PATH_VEN_ADD_FORM = "/vendor/addproductform";
const PATH_VEN_ORDERS = "/vendor/orders";

const PATH_MANU_DASH = "/manufacturer";       
const PATH_MANU_ADDPRO = "/manufacturer/addproducts"; 
const PATH_MANU_LISTPRODUCTS = "/manufacturer/listproducts";  
const PATH_MANU_VIEWORDERS= "/manufacturer/orders";      
const PATH_MANU_ISSUES = "/manufacturer/issues";
const PATH_MANU_PROUPDATE = "/manufacturer/updateproduct"
  return (
    <div>
      <Router>
        <Routes>
          <Route 
            path={BASE_PATH}
            element={
              <Home/>
            }
          >
            <Route path={PATH_HOME} element={<Homepage />} />
            <Route index element={<Homepage />} />
            <Route path={PATH_PRODUCTS} element={<ProductsList />} />
          
            <Route path={PATH_FOOTER} element={<Footer />} />
            <Route path={PATH_FORGETPASSWORD} element={<Forgetpassword />} />
            <Route path={PATH_RESETPASSWORD} element={<Resetpassword />} />

            <Route path={PATH_LOGIN} element={<Login />} />
            <Route path={PATH_SIGNUP} element={<SignUp />} />
            <Route path={PATH_WELCOME} element={ <Welcome /> }/>
            <Route path={PATH_ABOUTUS} element={ <Aboutus /> }/>
            
          </Route>

         {/* <Route path={PATH_USER} element={<User />}> */}
               <Route path={PATH_USERHEADER} element={<Userheader />}>
             <Route index element={<UserProfile />} />
              <Route path={PATH_USERPROFILE} element={<UserProfile />} />
              <Route path ={PATH_USERORDERS} element ={<UserOrders/>}/>
              <Route path ={PATH_USERBILLS} element ={<UserBills/>}/>
              <Route path ={PATH_USERWISH} element ={<UserWish/>}/>
              <Route path = {PATH_USERADDRESS} element = {<UserAddress/>}/>
              <Route path = {PATH_USERADDADDRESS} element = {<UserAddAddress/>}/>
              <Route path= {PATH_USER_CHECKOUT} element = {<Checkout/>}/>
              <Route path= {PATH_VEN_SHOP_PRO} element = {<VendorShopProducts/>}/>
              <Route path= {PATH_VEN_CHECKOUT} element = {<VendorCheckout/>}/>

          </Route>
          {/* </Route>  */}

          <Route path={PATH_MANU_DASH} element={<ManufactureDashboard />}>
          <Route index element={<ManufactureListProducts />} />
            <Route path={PATH_MANU_ADDPRO} element={<ManufactureAddProducts />} />
            <Route path={PATH_MANU_LISTPRODUCTS} element={<ManufactureListProducts />} />
            <Route path={PATH_MANU_VIEWORDERS} element={<ViewOrders />} />
            <Route path={PATH_MANU_ISSUES} element={<ManufactureIssues />} />
            <Route path={PATH_MANU_PROUPDATE} element={<ManufactureUpdateProduct/>}/>
          </Route>

          {/* <Route path={PATH_VEN_HOME} element={<VendorHome />}> */}
          <Route path={PATH_VEN_DASH} element={<VendorDashboard />} >
          <Route index element={<VendorHome />}/>
          <Route path={PATH_VEN_HOME} element={<VendorHome />}/>
          <Route path={PATH_VEN_ADD} element ={<VendorAddProducts />}/>
          <Route path={PATH_VEN_PRO} element ={<VendorProducts />}/>
          <Route path={PATH_VEN_ADD_FORM} element ={<VendorAddproductform />}/>
          <Route path={PATH_VEN_ORDERS} element ={<VendorOrders />}/>
          </Route>
          {/* <Route path={PATH_VEN_TABLES} element={<VendorTables />}>
           <Route path={PATH_VEN_HOME} element={<VendorHome />}>
           <Route path={PATH_VEN_DASH} element={<VendorHome />}>
            <Route path={PATH_VEN_LIST} element={<VendorListOrders />} /> */}
          {/* </Route> */}
        </Routes>
      </Router>
    </div>
  );
};

export default MainRoutes;
